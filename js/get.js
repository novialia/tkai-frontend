const tabel = document.getElementById('data-mahasiswa');
const url = 'https://';
fetch(url)
.then((resp) => resp.json())
.then(function(data) {
  let students = data.results;
  return students.map(function(student) {
    var tr = document.createElement('tr');
    var td1 = document.createElement('td');
    var td2 = document.createElement('td');
    var td3 = document.createElement('td');
    var td4 = document.createElement('td');

    var npm = document.createTextNode(student.npm);
    var name = document.createTextNode(student.name);
    var address = document.createTextNode(student.address);
    var phone = document.createTextNode(student.phone);

    td1.appendChild(npm);
    td2.appendChild(name);
    td3.appendChild(address);
    td4.appendChild(phone);
    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tabel.appendChild(tr);
  })
})
.catch(function(error) {
  console.log(JSON.stringify(error));
});